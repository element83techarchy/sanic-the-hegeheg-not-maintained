﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SanicOptionsMenu : MonoBehaviour
{
		private int menu = 0;
		public static SanicOptionsMenu options;
		public Resolution[] supportedResolutions;
		public int SelectedResolution = 0;
		//0 = General, 1 = Video, 2 = Controls
		void Start(){
			supportedResolutions = Screen.resolutions;
			SelectedResolution = PlayerPrefs.GetInt("Sanic_Screen_Res_Stuff_Not_Loomynarty");
		}
		KeyCode FetchPressedKey ()
		{
				int e = 330;
				for (int i = 0; i < e; i++) {
						if (i < 128 || i > 255) {
								KeyCode key = (KeyCode)i;
								if (Input.GetKeyDown (key)) {
					
										return key;
								}
						}
				}
		
				return KeyCode.None;
		}

		public void OptionsMenu (int windowID)
		{
				if (GUI.Button (new Rect (0, 20, 213, 24), "General")) {
						menu = 0;
				}
				if (GUI.Button (new Rect (214, 20, 213, 24), "Video")) {
						menu = 1;
				}
				if (GUI.Button (new Rect (427, 20, 212, 24), "Controls")) {
						menu = 2;
				}

				if (menu == 0) {
						GUI.Label (new Rect (240, 226, 160, 24), "Nothing here.");
				} else if (menu == 1) {

						//Debug.Log (supportedResolutions);
						GUI.Label (new Rect(10,50,80,24), "Resolution:");
						GUI.Box (new Rect (110,50,80,24), supportedResolutions[SelectedResolution].width + "x" + supportedResolutions[SelectedResolution].height);
						if(GUI.Button (new Rect (90, 50, 20, 24), "-")){
							if(SelectedResolution > 0){SelectedResolution--;}
							Debug.Log (SelectedResolution);
						}
						if(GUI.Button (new Rect (190, 50, 20, 24), "+")){
							if(SelectedResolution < supportedResolutions.Length - 1){SelectedResolution++;}
							Debug.Log (SelectedResolution);
						}

						if(GUI.Button (new Rect (560, 416, 80, 24), "Apply")){
							Screen.SetResolution (supportedResolutions[SelectedResolution].width, supportedResolutions[SelectedResolution].height, true);
								PlayerPrefs.SetInt("Sanic_Screen_Res_Stuff_Not_Loomynarty", SelectedResolution);
						}
				}else{
					GUI.Label(new Rect(0,50,640,72), "Move with A and D and jump with Space. Pause with ESCAPE. Also IDK how to implement rebindable controls. Sorry m80s, I'll do better in v0.12");
				}
		}

		void Awake ()
		{
				options = this;
		}
}
