﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {
	public Material NewMaterial;
	private Material OriginalMaterial;
	private bool enabled;
	private GameObject Sanic;
	public AudioClip checkpointSound;

	void Start(){
		OriginalMaterial = gameObject.renderer.material;
		Sanic = GameObject.FindGameObjectWithTag("Player");
	}

	void Update(){
		if(Sanic.GetComponent<SanicCharacterController>().respawnPoint == this.gameObject){
			enabled = true;
		}else{
			enabled = false;
		}

		if(enabled){
			gameObject.renderer.material = NewMaterial;
		}else{
			gameObject.renderer.material = OriginalMaterial;
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.gameObject.CompareTag ("Player")){
			if(other.gameObject.GetComponent<SanicCharacterController>().respawnPoint != this.gameObject){
				other.gameObject.GetComponent<SanicCharacterController>().respawnPoint = this.gameObject;
				Sanic.GetComponent<SanicCharacterController>().sanicCamera.audio.PlayOneShot (checkpointSound);
			}
		}
	}
}
