﻿using UnityEngine;
using System.Collections;

public class GameJoltAPIManager : MonoBehaviour {
	public int gameID;
	public static GameJoltAPIManager api;
	public string privateKey;
	public string userName;
	public string userToken;

	void Awake(){
		DontDestroyOnLoad (gameObject);
		if(api == null){
			api = this;
		}else{
			Destroy (this.gameObject);
		}
		GJAPI.Init (gameID, privateKey);
		GJAPI.Users.VerifyCallback += OnVerifyUser;
	}

	void OnVerifyUser(bool success){
		if(success){
			Debug.Log ("Sanic has successfully gotten on GameJolt. Saving the credentials...");
		}else{
			Debug.Log ("Sanic can't get on GameJolt. No weed will be smoked.");
		}
	}
}
