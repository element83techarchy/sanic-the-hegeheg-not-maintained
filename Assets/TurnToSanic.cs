﻿using UnityEngine;
using System.Collections;

public class TurnToSanic : MonoBehaviour
{
	public bool loggedIn = false;
	public bool ShowingGUI = false;
	public static TurnToSanic sanic;
	public GUISkin skin;
	private bool showOptions = false;
	private bool OptionsWindow = false;
	private string uname;
	private string utoken;

	void Update ()
	{
		if (Input.GetKey (KeyCode.Space)) {
			ShowingGUI = true;
		}
		if (PlayerPrefs.GetInt ("LoggedIn") == 1 && !loggedIn) {
			GJAPI.Users.Verify (PlayerPrefs.GetString ("Username"), PlayerPrefs.GetString ("UserKey"));
			GJAPI.Sessions.Open ();
			loggedIn = true;
		}
	}

	void insertCallbackHere(string uname, string utoken){
		GJAPI.Users.Verify (uname, utoken);
	}

	void Start ()
	{
		sanic = this;
	}

	void OnGUI ()
	{
		GUI.skin = skin;
		if (ShowingGUI) {
			if (!showOptions) {
				if (GUI.Button (new Rect (Screen.width / 2 - 192, Screen.height / 2 + 192, 96, 96), "Play")) {
					Application.LoadLevel (1);
				}
				if (GUI.Button (new Rect (Screen.width / 2 - 96, Screen.height / 2 + 192, 96, 96), "Options", skin.GetStyle("Right"))) {
					OptionsWindow = true;
				}
				if (GUI.Button (new Rect (Screen.width / 2, Screen.height / 2 + 192, 96, 96), "Blog")) {
					Application.OpenURL ("http://gamejolt.com/games/sanic-the-hegehog/news/30460/");
				}
				if(!Application.isWebPlayer){
					if (GUI.Button (new Rect (Screen.width / 2 + 96, Screen.height / 2 + 192, 96, 96), "Quit", skin.GetStyle("Right"))) {
						GJAPI.Sessions.Close ();
						Application.Quit ();
					}
				}
			}
			if (!loggedIn) {
				if (GUI.Button (new Rect (0, 0, 96, 96), "GameJolt", skin.GetStyle ("Right"))) {
					GJAPIHelper.Users.ShowLogin ();
				}
			} else {
				GUI.Label (new Rect (0, 0, 500, 30), PlayerPrefs.GetString ("Username"));
					if (GUI.Button (new Rect (0, 31, 96, 96), "Logout", skin.GetStyle ("Right"))) {
						PlayerPrefs.DeleteKey ("Username");
						PlayerPrefs.DeleteKey ("UserKey");
						PlayerPrefs.DeleteKey ("LoggedIn");
					}
			}
			GUI.Label (new Rect (Screen.width - 350, 0, 350, 30), "Most things made by b Studios.");
			GUI.Label (new Rect (Screen.width - 350, 30, 350, 30), "Co-development by NAR Company.");
			GUI.Label (new Rect (Screen.width - 350, 60, 350, 30), "Sonic made by Sega.");
			GUI.Label (new Rect (Screen.width - 350, 90, 350, 30), "All credit goes to all authors.");
			if (OptionsWindow) {
				GUI.Window (420, new Rect (Screen.width / 2 - 320, Screen.height / 2 - 240, 640, 480), SanicOptionsMenu.options.OptionsMenu, "Settings");
				if (GUI.Button (new Rect (Screen.width / 2 + 320, Screen.height / 2 - 240, 24, 24), "X")) {
					OptionsWindow = false;
				}
			}
		} else {
			GUI.Label (new Rect (Screen.width / 2 - 100, Screen.height / 2 + 150, 200, 666), "Press Spacebar");
		}
	}
}
