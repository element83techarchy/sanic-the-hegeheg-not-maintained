﻿using UnityEngine;
using System.Collections;

public class SanicCamera : MonoBehaviour {
	public GameObject sanic;
	private bool paused;
	public GUISkin Skin;
	public GUIStyle black;
	public bool shrekt;
	public Texture2D sanicIcon;
	public Texture2D bleck;
	void Start(){
		GJAPI.Trophies.Add (9816);
	}
	void Update(){
		if(Input.GetKeyDown (KeyCode.Escape)){
			paused = true;
		}
		if(paused){
			Time.timeScale = 0f;
		}else{
			Time.timeScale = 1f;
		}

		if(Input.GetKeyDown (KeyCode.Space)&&shrekt){
			Application.LoadLevel (Application.loadedLevel);
		}
		if(Input.GetKeyDown (KeyCode.Escape)&&shrekt){
			Application.LoadLevel (0);
		}

	}
	void OnGUI(){
		GUI.skin = Skin;
		GUI.Box (new Rect(0,0,64,64), sanicIcon);
		GUI.Label (new Rect(65,0,100,32), "Sanic x" + sanic.GetComponent<SanicCharacterController>().lives);
		GUI.Label (new Rect(65,33,100,32), "Rings: " + sanic.GetComponent<SanicCharacterController>().rings);
		if(paused){
			if(GUI.Button (new Rect (0, Screen.height / 2 - 96, 96, 96), "Resume")){
				paused = false;
			}
			if(GUI.Button (new Rect (0, Screen.height / 2, 96, 96), "Quit")){
				Application.LoadLevel(0);
			}
		}
		if(shrekt){
			GUI.Box (new Rect(0,0,Screen.width, Screen.height), bleck, black);
			GUI.Label (new Rect(Screen.width / 2 - 70, Screen.height / 2 - 18, 140, 36), "GAME OVER");
			GUI.Label (new Rect(Screen.width / 2 - 55, Screen.height / 2 + 18, 110, 36), "Restart?");
			GUI.Label (new Rect(0, Screen.height - 36, 120, 36), "ESC = NO");
			GUI.Label (new Rect(Screen.width - 140, Screen.height - 36, 140, 36), "SPACE = YES");
			audio.Stop ();
		}
	}
}
