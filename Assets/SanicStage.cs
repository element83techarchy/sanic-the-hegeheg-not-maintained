﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class SanicStage : MonoBehaviour {
	public string Zone = "Sanic zone";
	public GameObject sanic;
	public int Act = 1;
	public AudioClip StageSound;

	void Start(){
		audio.clip = StageSound;
		audio.Play ();
	}
}
