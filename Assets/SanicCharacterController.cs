﻿using UnityEngine;
using System.Collections;

public class SanicCharacterController : MonoBehaviour
{
		public float maxSpeeds;
		public float currentSpeeds;
		public AudioClip ringSound;
		public AudioClip jumpSound;
		private float groundDistance;
		public float jumpHeight;
		public GameObject sanicCamera;
		public GameObject respawnPoint;
		public GameObject ring;
		public int rings;
		private float input = 0f;
		public int lives = 3;
		
		void Start ()
		{
				groundDistance = collider.bounds.extents.y;
		}

		bool isGrounded ()
		{
				return Physics.Raycast (transform.position, -Vector3.up, groundDistance + 0.01f);
		}

		void Update ()
		{
				input = RebindableInput.GetAxis ("Horizontal");
				rigidbody.AddRelativeForce (Time.deltaTime * currentSpeeds * Input.GetAxis ("Horizontal"), 0, 0, ForceMode.Force);
				if (RebindableInput.GetKeyDown ("Jump") && isGrounded ()) {
						rigidbody.AddForce (Vector3.up * jumpHeight);
						sanicCamera.audio.PlayOneShot (jumpSound);
				}
				if (RebindableInput.GetAxis ("Horizontal") < 0) {
						transform.localScale = new Vector3 (-1, 1, 1);
				} else {
						transform.localScale = new Vector3 (1, 1, 1);
				}
		}

		void FixedUpdate ()
		{

				if (currentSpeeds < maxSpeeds && currentSpeeds >= 0 && input != 0) {
						currentSpeeds += 100.0f * Time.deltaTime * 10;
				} else {
						if (currentSpeeds > 0) {
								currentSpeeds -= 200.0f * Time.deltaTime * 10;
						}
				}
		
				if (currentSpeeds < 0) {
						currentSpeeds = 0;
				}
		

		


		}

		void OnTriggerEnter (Collider other)
		{
				if (other.gameObject.CompareTag ("Ring")) {
						rings++;
						Destroy (other.gameObject);
						sanicCamera.audio.PlayOneShot (ringSound);
				} else if (other.gameObject.CompareTag ("AutoRespawn")) {	
					GetShrekt (true);
				}
		}

		void GetShrekt (bool Insta)
		{
				if (lives > 0) {
						if (rings == 0 || Insta) {
								transform.position = respawnPoint.transform.position;
								currentSpeeds = 0;
								rings = 0;
								lives--;
						} else {
								for (int i = 0; i < rings; i++) {
										Instantiate (ring, new Vector3 (transform.position.x + Random.Range (1, 4), transform.position.y + Random.Range (-4, 4), 0), Quaternion.identity);
										rings--;
								}
						}
				}else{
			sanicCamera.GetComponent<SanicCamera>().shrekt = true;
				}
		}
}
