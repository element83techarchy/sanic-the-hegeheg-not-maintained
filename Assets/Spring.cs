﻿using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour {
	private GameObject cam;
	public AudioClip springSound;
	void Start(){
		cam = GameObject.Find("Main Camera");
	}
	void OnCollisionEnter(Collision col){
		if(col.gameObject.tag == "Player"){
			col.gameObject.rigidbody.AddForce (new Vector3(0,500,0));
			cam.audio.PlayOneShot (springSound);
		}
	}
}
